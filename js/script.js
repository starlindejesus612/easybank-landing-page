const icon = document.getElementById("icon")


const imgIcon = (icon) =>{
    if(icon) return "/images/icon-close.svg";
    else return "/images/icon-hamburger.svg"
}
icon.onclick = (e) =>{
  icon.nextElementSibling.classList.toggle("visible")
  icon.src = imgIcon(e.target.nextElementSibling.classList.contains("visible"))
}